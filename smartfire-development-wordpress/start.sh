#!/usr/bin/env bash

PORT=${PORT:-8888}
WP_PATH=${WP_PATH:-/var/www/html}
WP_DATABASE_NAME=${WP_DATABASE_NAME:-wordpress}
WP_DATABASE_USERNAME=${WP_DATABASE_USERNAME:-root}
WP_DATABASE_PASSWORD=${WP_DATABASE_PASSWORD:-root}
WP_DATABASE_HOST=${WP_DATABASE_HOST:-mysql}
WP_ADMIN_USERNAME=${WP_ADMIN_USERNAME:-admin}
WP_ADMIN_PASSWORD=${WP_ADMIN_PASSWORD:-admin}
SQL_TIMEOUT=${SQL_TIMEOUT:-60}

COUNTER=0
while ! mysqladmin ping -p"${WP_DATABASE_PASSWORD}" -h"${WP_DATABASE_HOST}" --silent; do
    COUNTER=$[$COUNTER +1]
    if [[ ${COUNTER} -gt ${SQL_TIMEOUT} ]]; then
        echo "Unable to ping the mysql server"
        exit 1
    fi
    echo "Waiting for mysql"
    sleep 1
done

echo "MySQL OK"

cat <<PHP > ${WP_PATH}/wp-config-database.php
<?php


/** The name of the database for WordPress */
define( 'DB_NAME', '${WP_DATABASE_NAME}' );

/** MySQL database username */
define( 'DB_USER', '${WP_DATABASE_USERNAME}' );

/** MySQL database password */
define( 'DB_PASSWORD', '${WP_DATABASE_PASSWORD}' );

/** MySQL hostname */
define( 'DB_HOST', '${WP_DATABASE_HOST}' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );
PHP

echo "Starting wp-cli"
wp --allow-root server --host=0.0.0.0 --port="${PORT}" --path="${WP_PATH}"

