#!/usr/bin/env bash
set -e

PHP_FPM_HOST=${PHP_FPM_HOST:-127.0.0.1:9000}
sed -i "s/fastcgi_pass 127.0.0.1:9000;/fastcgi_pass ${PHP_FPM_HOST};/g" /etc/nginx/nginx.conf
/usr/sbin/nginx
