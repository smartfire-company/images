# Smartfire Docker Images

This repository contains Docker images used accross Smartfire projects. They
are automaticaly build by the Gitlab CI.

## Authentication
Using them require to authentificate agianst Docker image repository `gitlab.com`

1. Create a private token on: https://gitlab.com/profile/personal_access_tokens
   with the following rights:
    - API (if you want to be able to push)
    - read_registry
2. Use it to login with:
    ```
    docker login registry.gitlab.com
    ```

## How to create a new image
1. Create a new folder in this repository
2. Add a Dockerfile in it
3. Add a `version` file containing the version number (e.g. `1.0`)
   All image are tagged with `${CURRENT_YEAR}.${VERSION}` see 
   https://calver.org/ for more details
4. Eventually create a `dependencies` file and list dependency of this image:
   ```
   wordpress
   database
   ```
   The image will be build only after wordpress and database image have been built.
5. Push it on a `feature/${my_new_image_name}` branch and wait for merge
