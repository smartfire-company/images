#!/bin/bash

REPOSITORY=registry.gitlab.com/smartfire-company/images
PUSH=${PUSH:-`false`}


function contains() {
    if [[ $1 =~ "(^|[[:space:]])${2}($|[[:space:]])" ]]; then
        return 0
    else
        return 1
    fi
}

function build() {
    name=$1
    ver="1.0"
    if [[ -e ${name}/version ]]; then
        ver="`cat ${name}/version`"
    fi
    tag=${REPOSITORY}/${name}:${ver}
    echo "Building ${tag} ..."
    cd ${name}
    docker pull ${tag} || echo "No previous version available"
    docker build -t ${tag} .
    if [[ "${PUSH}" = true ]]; then
        docker push ${tag} || (echo "Unable to push the image, please check the logs" && exit 1)
    fi
    cd ..
}

images=`find -maxdepth 2 -type f -name 'Dockerfile' | sed -r 's|/[^/]+$||' | sed 's/\.\///g' | sort | uniq`
deps=""
first_loop=1
for image in ${images} ; do
    if [[ -n "${1}" ]]; then
        if [[ ${1} = ${image} ]]; then
            echo "Found $image to build"
        else
            echo "Skipped $image"
            continue;
        fi
    fi
    (( $first_loop )) && deps="${image} ${image}" || deps="${deps}"$'\\n'"${image} ${image}"
    unset first_loop
    if [[ -e ${image}/dependencies ]]; then
        for dep in `cat ${image}/dependencies` ; do
            if `echo ${images} | grep -q  ${dep}`; then
                deps="${deps}"$'\\n'"${dep} ${image}"
            else
                echo "Image ${dep} does not exist"
                exit 1
            fi
        done
    fi
done
for image in `echo -e ${deps} | tsort` ; do
    build ${image}
done

